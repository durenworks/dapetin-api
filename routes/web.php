<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//your api routes in here


$router->get('/', function () use ($router) {
    return $router->app->version();

});
$router->get("/download", "DownloadController@index");
$router->get("/clear_test_contract", "ContractController@clearAmTestContract");
$router->group(['middleware' => 'cors'], function($router)
{
  $router->post("/login", "AuthController@login");
  $router->post("/profile", "AuthController@get_profile");


  //-------------------------- OPEN ROUTE --------------------------------------

  $router->get("/segment/", "SegmentController@index");
  $router->get("/am/{id}[/{keyword}]", "AMController@index");
  $router->get("/statistics[/{token}]", "StatisticsController@index");
  $router->get("/service_type/", "AMController@servicetype");
  $router->get("/service/{id}", "AMController@service");
  $router->get('bot/sendmessage', "telegramController@sendMe");
  $router->get('/customer/{token}[/{keyword}]', "CustomerController@index");
  $router->get('/service/search/{token}[/{keyword}]', "ServicesController@index");
  $router->get('/reminder/emailtelegram/{emailOn}/{telegramOn}', "ReminderController@index");
  $router->get('/reminder/email', "ReminderController@email");
  $router->post('/clear_user', "Superadmin\UserController@clearUser");
  $router->post('/restore_user', "Superadmin\UserController@restoreUser");
  //--------------------------- SUPERADMIN ROUTE -------------------------------

  // ------------------ Manager and AM Managemnt -------------------------------
  $router->post("/superadmin/user/store/", "Superadmin\UserController@store");
  $router->post("/superadmin/user/", "Superadmin\UserController@index");
  $router->post("/superadmin/user/detail/", "Superadmin\UserController@detail");
  $router->post("/superadmin/user/update/", "Superadmin\UserController@update");
  $router->post("/superadmin/user/delete/", "Superadmin\UserController@delete");
  $router->post("/superadmin/user/restore/", "Superadmin\UserController@restore");

  // ------------------ Admin Management ---------------------------------------
  $router->post("/superadmin/admin/store", "Superadmin\AdminController@store");
  $router->post("/superadmin/admin", "Superadmin\AdminController@index");
  $router->post("/superadmin/admin/detail/", "Superadmin\AdminController@detail");
  $router->post("/superadmin/admin/update/", "Superadmin\AdminController@update");
  $router->post("/superadmin/admin/delete/", "Superadmin\AdminController@delete");

  // ------------------ Service Management --------------------------------------
  $router->post("/superadmin/service/store", "Superadmin\ServiceController@store");
  $router->post("/superadmin/service/", "Superadmin\ServiceController@index");
  $router->post("/superadmin/service/detail/", "Superadmin\ServiceController@detail");
  $router->post("/superadmin/service/update/", "Superadmin\ServiceController@update");
  $router->post("/superadmin/service/delete/", "Superadmin\ServiceController@delete");

  // ----------------- Contract Management -------------------------------------
  $router->post("/contract/search/", "ContractController@index");
  $router->get("/contract/", "ContractController@index");
  $router->post("/contract/do", "ContractController@do");
  $router->post("/contract/store", "ContractController@store");
  $router->get("/contract/recent_activities", "ContractController@recent");
  $router->get("/contract/recent_new", "ContractController@recentNew");
  $router->post("/contract/reminder", "ContractController@reminder");
  $router->post("/contract/reminder", "ContractController@reminder");
  $router->post("/contract/detail", "ContractController@detail");
  $router->post("/contract/amandemen", "ContractController@amandemen");
  $router->post("/contract/service/do", "ContractController@service_do");
});
