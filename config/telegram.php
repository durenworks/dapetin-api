<?php

return [

    //  'bot_token'&nbsp;=>&nbsp;'731078417:AAG0DdxALkIXkc4HLqNr6',


    'bot_token' => env('TELEGRAM_BOT_TOKEN', 'YOUR-BOT-TOKEN'),


    'async_requests' => env('TELEGRAM_ASYNC_REQUESTS', false),

    'http_client_handler' => null,

    'commands' => [
        Telegram\Bot\Commands\HelpCommand::class,
    ],
];
