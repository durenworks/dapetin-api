<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nik')->unique()->nullable();
            $table->string("email")->unique();
            $table->string("password");
            $table->string("name");
            $table->string("phone")->unique();
            $table->enum("level", ['SUPERADMIN', 'MANAGER', 'ACCOUNT MANAGER'])->nullable();
            $table->unsignedBigInteger("segment_id")->nullable();
            $table->string("telegram_name")->nullable();
            $table->string("telegram_id")->nullable();
            $table->enum("witel", ['SAMARINDA', 'KALTARA', 'BALIKPAPAN', 'KALBAR', 'KALTENG', 'KALSEL']);
            $table->dateTime('last_active')->nullable();
            $table->boolean('is_active')->default(1);
            $table->string("token")->nullable();
            $table->timestamps();

            $table->foreign('segment_id')->references('id')->on('segments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
