<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPhoneColumnOnContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function (Blueprint $table) {
            //
            $table->string('customer_pic2_name')->nullable()->change();
            $table->string('customer_pic2_position')->nullable()->change();
            $table->string('customer_pic2_email')->nullable()->change();
            $table->string('customer_pic1_phone')->nullable();
            $table->string('customer_pic2_phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function (Blueprint $table) {
            //
            $table->dropColumn('customer_pic2_phone');
            $table->dropColumn('customer_pic1_phone');
        });
    }
}
