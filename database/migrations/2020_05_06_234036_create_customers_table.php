<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('npwp');
            $table->string('address');
            $table->string('pic_1_name');
            $table->string('pic_1_position')->nullable();
            $table->string('pic_1_email')->nullable();
            $table->string('pic_1_phone')->nullable();
            $table->string('pic_2_name')->nullable();
            $table->string('pic_2_position')->nullable();
            $table->string('pic_2_email')->nullable();
            $table->string('pic_2_phone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
