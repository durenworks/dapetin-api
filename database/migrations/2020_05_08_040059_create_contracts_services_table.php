<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sid');
            $table->unsignedBigInteger('contract_id');
            $table->unsignedBigInteger('service_type_id');
            $table->unsignedBigInteger('service_id');
            $table->string('package');
            $table->string('bandwith_package');
            $table->decimal('instalation_fee', 12, 2);
            $table->decimal('monthly_fee', 12,2);
            $table->text('note');
            $table->timestamps();

            $table->foreign('contract_id')->references('id')->on('contracts');
            $table->foreign('service_type_id')->references('id')->on('service_types');
            $table->foreign('service_id')->references('id')->on('services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts_services');
    }
}
