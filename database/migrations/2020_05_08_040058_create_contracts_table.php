<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('contract_number');
            $table->unsignedBigInteger('segment_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('customer_id');
            $table->string('customer_name');
            $table->string('customer_npwp');
            $table->string('customer_address');
            $table->string('customer_pic1_name');
            $table->string('customer_pic1_position');
            $table->string('customer_pic1_email');
            $table->string('customer_pic2_name');
            $table->string('customer_pic2_position');
            $table->string('customer_pic2_email');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('duration');
            $table->string('activities')->default("Input Contract");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
