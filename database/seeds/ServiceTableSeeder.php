<?php

use Illuminate\Database\Seeder;
use App\Models\Service;
use App\Models\ServiceType;

class SerivceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ServiceType = ServiceType::where('name', 'CONNECTIVITY')->first();
        $connectivityId = $ServiceType->id;

        $ServiceType = ServiceType::where('name', 'CPE')->first();
        $cpeId = $ServiceType->id;

        Service::create([
            'name' => 'ASTINET DEDICATED',
            'service_type_id' => $connectivityId
        ]);

        Service::create([
            'name' => 'ASTINET LITE',
            'service_type_id' => $connectivityId
        ]);

        Service::create([
            'name' => 'ASTINET BB',
            'service_type_id' => $connectivityId
        ]);

        Service::create([
            'name' => 'WIFI STATION',
            'service_type_id' => $connectivityId
        ]);

        Service::create([
            'name' => 'INDIHOME',
            'service_type_id' => $connectivityId
        ]);

        Service::create([
            'name' => 'VPN IP',
            'service_type_id' => $connectivityId
        ]);

        Service::create([
            'name' => 'METRO E',
            'service_type_id' => $connectivityId
        ]);

        Service::create([
            'name' => 'MANGOESKY',
            'service_type_id' => $cpeId
        ]);

        Service::create([
            'name' => 'RADIO IP',
            'service_type_id' => $cpeId
        ]);

        Service::create([
            'name' => 'COLLOCATION',
            'service_type_id' => $cpeId
        ]);

        Service::create([
            'name' => 'M2M',
            'service_type_id' => $cpeId
        ]);
    }
}
