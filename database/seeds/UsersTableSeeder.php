<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Superadmin',
            'email' => 'superadmin@example.com',
            'level' => 'SUPERADMIN',
            'phone' => '0856xxxxxx',
            'password' => Hash::make('superpass'),
        ]);

        User::create([
            'name' => 'AM1 Test',
            'nik' => '123456',
            'email' => 'am1test@example.com',
            'level' => 'ACCOUNT MANAGER',
            'telegram_name' => 'am1test',
            'telegram_id' => '1117901595',
            'segment_id' => '1',
            'phone' => '0857xxxxxx',
            'password' => Hash::make('superpass'),
        ]);

        User::create([
            'name' => 'AM2 Test',
            'nik' => '234567',
            'email' => 'am2test@example.com',
            'level' => 'ACCOUNT MANAGER',
            'telegram_name' => 'am2test',
            'telegram_id' => '1117901595',
            'segment_id' => '2',
            'phone' => '0858xxxxxx',
            'password' => Hash::make('superpass'),
        ]);

        User::create([
            'name' => 'Manager Test',
            'email' => 'manajer1test@example.com',
            'nik' => '345678',
            'level' => 'MANAGER',
            'telegram_name' => 'manajer1test',
            'telegram_id' => '1117901595',
            'segment_id' => '1',
            'phone' => '0859xxxxxx',
            'password' => Hash::make('superpass'),
        ]);

        User::create([
            'name' => 'Manager Test',
            'nik' => '456789',
            'email' => 'manajer2test@example.com',
            'level' => 'MANAGER',
            'telegram_name' => 'manajer2test',
            'telegram_id' => '1117901595',
            'segment_id' => '2',
            'phone' => '08510xxxxxx',
            'password' => Hash::make('superpass'),
        ]);

    }
}
