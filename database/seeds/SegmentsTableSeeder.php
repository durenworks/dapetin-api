<?php

use Illuminate\Database\Seeder;
use App\Models\Segment;

class SegmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Segment::create([
            'name' => 'Business Services',
            'acronym' => 'BS'
        ]);

        Segment::create([
            'name' => 'Enterprise Services',
            'acronym' => 'ES'
        ]);

        Segment::create([
            'name' => 'Government Services',
            'acronym' => 'GS'
        ]);
    }
}
