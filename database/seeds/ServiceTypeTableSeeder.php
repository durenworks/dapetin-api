<?php

use Illuminate\Database\Seeder;
use App\Models\Segment;
use App\Models\ServiceType;

class ServiceTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServiceType::create([
            'name' => 'CONNECTIVITY'
        ]);

        ServiceType::create([
            'name' => 'CPE'
        ]);
    }
}
