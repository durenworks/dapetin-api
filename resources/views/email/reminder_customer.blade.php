<p><img style="float: right;" src="https://i.ibb.co/N1wHcsV/Whats-App-Image-2020-06-02-at-10-23-57-AM.jpg" alt="https://ibb.co/VHXPftj&quot;&gt;&lt;img src=&quot;https://i.ibb.co/N1wHcsV/Whats-App-Image-2020-06-02-at-10-23-57-AM.jpg" width="138" height="99" /></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style="color: #ff0000;">PENTING</span></p>
<p>Yth, <strong>{{$data->customer_pic1_name}}</strong></p>
<p>Dengan Hormat</p>
<p>Terima kasih kami ucapkan atas kepercayaan Saudara yang telah memilih Telkom Group sebagai Mitra dalam pemenuhan kebutuhan ICT pada <strong>{{$data->customer_name}}</strong></p>
<p>Bersama ini kami sampaikan Reminder Kontrak yang akan habis pada tanggal <strong>{{$data->end_date_indo}}</strong> dengan Nama Pelanggan {{$data->customer_name}} untuk dapat Saudara lakukan perpanjangan kontrak sebelum tanggal jatuh tempo yang telah ditentukan.</p>
<p>Demi kemudahan dan percepatan dalam melakukan identifikasi atas perpanjangan kontrak yang Saudara akan lakukan serta untuk menghindari layanan terisolir dikarenakan keterlambatan perpanjangan kontrak mohon kerjsamanya untuk melakukan perpanjangan kontrak dengan data layanan sebagai berikut</p>
<table border="1" cellspacing="0" cellpadding="5">
<tbody>
<tr>
<td><strong>No</strong></td>
<td><strong>Alamat Instalasi</strong></td>
<td><strong>Layanan</strong></td>
<td><strong>Bandwith</strong></td>
<td><strong>SID</strong></td>
</tr>
<?php $i = 1; ?>
<?php foreach ($data->services as $key => $service): ?>
  <tr>
  <td>{{ $i }}</td>
  <td>{{ $data->customer_address }}</td>
  <td>{{ $service->package }}</td>
  <td>{{ $service->bandwith_package }}</td>
  <td>{{ $service->sid }}</td>
  </tr>
  <?php $i++; ?>
<?php endforeach; ?>
</tbody>
</table>
<p>&nbsp;</p>
<p>Apabila memerlukan informasi lebih lanjut terkait dengan kontrak perpanjangan, mohon dapat menghubungi <strong>{{$data->user->name}}</strong> melalui  telepon <strong>{{$data->user->phone}}</strong> atau email <strong>{{$data->user->email}}</strong> yang telah ditunjuk.</p>
<p>Demikian disampaikan atas perhatian Saudara kami ucapkan terimaksaih</p>
<p>Hormat Kami,</p>
<p>&nbsp;</p>
<p>PT Telekomunikasi Indonesia, Tbk</p>
