<?php

namespace App\Http\Controllers;

use App\Models\Segment;
use App\Models\User;
use App\Models\ServiceType;
use App\Models\Service;
use Illuminate\Http\Request;

class AMController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index($id = 1, $keyword = "")
    {
        $query = User::select('id', 'name')
                        ->where('level', 'ACCOUNT MANAGER')
                        ->where('name', 'like', '%'. $keyword .'%')
                        ->where('segment_id', $id);

        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message" => "Something Wrong",
                "data" => []
            ];
        }

        return response()->json($out);
    }

    public function servicetype()
    {
        $query = ServiceType::active();

        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message" => "Something Wrong",
                "data" => []
            ];
        }

        return response()->json($out);
    }

    public function service(Request $request)
    {
      $type_id = $request->id;
        $query = Service::active()
                  ->where('service_type_id', $type_id);

        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message" => "Something Wrong",
                "data" => []
            ];
        }

        return response()->json($out);
    }

    //
}
