<?php

namespace App\Http\Controllers;

use App\Models\Contract;
use App\Models\ContractService;
use App\Models\Customer;
use App\Models\User;

class StatisticsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index($token = "")
    {
        $userId = "";
        $segmentId = "";
        $level = "";

        if ($token != "") {
          $user = User::where('token', $token)->first();
          $userId = $user->id;
          $segmentId = $user->segment_id;
          $level = $user->level;
        }

        $totalContract = Contract::where('contract_number', '<>', null);
        if ($level == 'ACCOUNT MANAGER') {
          $totalContract = $totalContract->where('user_id', $userId);
        }

        if ($level == 'MANAGER') {
          $totalContract = $totalContract->where('segment_id', $segmentId);
        }
        $totalContract = $totalContract->count();

        $totalCustomer = Contract::where('contract_number', '<>', null);
        if ($level == 'ACCOUNT MANAGER') {
          $totalCustomer = $totalCustomer->where('user_id', $userId);
        }

        if ($level == 'MANAGER') {
          $totalCustomer = $totalCustomer->where('segment_id', $segmentId);
        }
        $totalCustomer = $totalCustomer->distinct()->count('customer_id');

        $totalService = ContractService::where('sid', '<>', null);
        if ($level == 'ACCOUNT MANAGER') {
          $totalService = $totalService->where(function($query) use ($userId){
              $query->whereHas('contract', function ($query) use($userId){
                  return $query->where('user_id', $userId);
              });
            });
          }

        if ($level == 'MANAGER') {
          $totalService = $totalService  ->where(function($query) use ($segmentId){
              $query->whereHas('contract', function ($query) use($segmentId){
                  return $query->where('segment_id', $segmentId);
              });
            });
        }
        $totalService = $totalService->count();

        $data = [
            "totalContract" => $totalContract,
            "totalCustomer" => $totalCustomer,
            "totalService" => $totalService
        ];

        $out = [
            "status"    => 'success',
            "message" => 'success',
            "data" => $data,
        ];

        return response()->json($out);
    }
    //
}
