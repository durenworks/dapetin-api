<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contract;
use App\Http\Controllers\Controller;
use App\Models\ContractService;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index($token, $keyword = "")
    {

      if ($token != "") {
        $user = User::where('token', $token)->first();
        $userId = $user->id;
        $segmentId = $user->segment_id;
        $level = $user->level;
      }
      else {
        $out = [
            "status"    => 'failed',
            "message" => "Something Wrong",
            "data" => []
        ];

        return response()->json($out);
      }

        $query  = Customer::where('name', 'like', '%' . $keyword . '%');
        if ($level == 'ACCOUNT MANAGER') {
          $query = $query->whereHas('contracts', function ($query) use($userId){
            return $query->where('user_id', $userId);
          });;
        }

        if ($level == 'MANAGER') {
          $query = $query->whereHas('contracts', function ($query) use($segmentId){
            return $query->where('segment_id', $segmentId);
          });;
        }

        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message" => "Something Wrong",
                "data" => []
            ];
        }

        return response()->json($out);
    }
//
}
