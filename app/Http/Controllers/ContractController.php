<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contract;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Models\ContractService;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class ContractController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $itemPerPage = 10;
        $keyword = $request->keyword;

        $token = $request->input('token');
        $status = $request->input('status');
        $page = $request->input('page');

        $user = User::where('token', $token)->first();
        $userId = $user->id;
        $segmentId = $user->segment_id;
        $level = $user->level;

        $query = Contract::where(function($q) use ($keyword){
                              $q->where(function($query) use ($keyword){
                                $query->orWhere('customer_pic1_name', 'like', '%' . $keyword . '%');
                                $query->orWhere('customer_pic2_name', 'like', '%' . $keyword . '%');
                                $query->orWhere('customer_name', 'like', '%' . $keyword . '%');
                              });
                              $q->orwhere(function($query) use ($keyword){
                                $query->where('contract_number', 'like', '%' . $keyword . '%');
                                $query->orwhereHas('services', function ($query) use($keyword){
                                  $query->where('sid', 'like', '%' . $keyword . '%');
                                });
                              });
                              $q->orwhere(function($query) use ($keyword){
                                $query->orwhereHas('user', function ($query) use($keyword){
                                  $query->where('name', 'like', '%' . $keyword . '%');
                                });
                              });
                            })
                            ->orderBy('updated_at', 'desc')
                            ->with('segment:id,acronym,name')
                            ->with('user:id,name');
                            //->with('services')->with('service:name')->with('serviceType:name');

        if ($level == 'ACCOUNT MANAGER') {
          //var_dump($segmentId);die();
          $query = $query->where('user_id', $userId);
          $query = $query->where('segment_id', $segmentId);
        }

        if ($level == 'MANAGER') {
          $query = $query->where('segment_id', $segmentId);
        }

        if (isset($status)) {
          if ($status) {
            $query = $query->where('is_do', 0);
          }

          if (!$status) {
            $query = $query->where('is_do', 1);
          }
        }

        $count = $query->count();
        $totalPage = ceil($count / $itemPerPage);

        $arrPage = [];
        for ($i = 1; $i <= $totalPage; $i++) {
            array_push($arrPage, $i);
        }

        if (isset($page)) {
            $skip = ($page - 1) * $itemPerPage;
            $query = $query->skip($skip)->take($itemPerPage);
        }

        if ($data = $query->get()) {
            $count = $query->count();
            $totalPage = ceil($count / $itemPerPage);
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "totalPage" => $totalPage,
                "arrayPage" => $arrPage,
                "data" => $data
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message" => "Something Wrong",
                "data" => []
            ];
        }

        return response()->json($out);
    }

    public function store(Request $request)
    {
        $contracNumber = $request->input("contract_number");
        $segment_id = $request->input("segment_id");
        $am_id = $request->input("am_id");
        $customerName = $request->input("customer_name");
        $npwp = $request->input("customer_npwp");
        $address = $request->input("customer_address");
        $pic1Name = $request->input("customer_pic1_name");
        $pic1Email = $request->input("customer_pic1_email");
        $pic1Position = $request->input("customer_pic1_position");
        $pic1Phone = $request->input("customer_pic1_phone");
        $pic2Name = $request->input("customer_pic2_name");
        $pic2Email = $request->input("customer_pic2_email");
        $pic2Position = $request->input("customer_pic2_position");
        $pic2Phone = $request->input("customer_pic2_phone");
        $duration = $request->input("duration");
        $startDate = $request->input("start_date");
        $endDate = $request->input("end_date");

        $services = $request->input("services");

        DB::beginTransaction();
        try{
            //var_dump($services);die();
            $customer = Customer::select("id")
                                ->where('name', $customerName)
                                ->where('npwp', $npwp)
                                ->first();

            if ($customer == null) {
                $dataCustomer = [
                    "name" => $customerName,
                    "npwp" => $npwp,
                    "address" => $address,
                    "pic_1_name" => $pic1Name,
                    "pic_1_email" => $pic1Email,
                    "pic_1_position" => $pic1Position,
                    "pic_1_phone" => $pic1Phone,
                    "pic_2_name" => $pic2Name,
                    "pic_2_email" => $pic2Email,
                    "pic_2_position" => $pic2Position,
                    "pic_2_phone" => $pic2Phone,
                ];

                $customerId = Customer::create($dataCustomer)->id;
            }
            else {
                $customerId = $customer->id;
            }

            $dataContract = [
                "contract_number" => $contracNumber,
                "segment_id" => $segment_id,
                "user_id" => $am_id,
                "customer_id" => $customerId,
                "customer_name" => $customerName,
                "customer_npwp" => $npwp,
                "customer_address" => $address,
                "customer_pic1_name" => $pic1Name,
                "customer_pic1_email" => $pic1Email,
                "customer_pic1_position" => $pic1Position,
                "customer_pic1_phone" => $pic1Phone,
                "customer_pic2_name" => $pic2Name,
                "customer_pic2_email" => $pic2Email,
                "customer_pic2_position" => $pic2Position,
                "customer_pic2_phone" => $pic2Phone,
                "start_date" => $startDate,
                "end_date" => $endDate,
                "duration" => $duration
            ];

        //var_dump($dataContract);die();
            //var_dump($services);die();
            if ($contract = Contract::create($dataContract)) {

                foreach($services as $value) {
                  //var_dump($value);die();
                    $dataService = [
                        "sid" => $value['sid'],
                        "service_type_id" => $value['service_type_id'],
                        "service_id" => $value['service_id'],
                        "address" => array_key_exists('address', $value) ? $value['address'] : "",
                        "package" => $value['package'],
                        "bandwith_package" => $value['bandwith_package'],
                        "instalation_fee" => $value['instalation_fee'],
                        "monthly_fee" => $value['monthly_fee'],
                        "note" => $value['note'],
                    ];
                    //var_dump($dataService);die();
                    $contractService = new ContractService($dataService);
                    $contract->services()->save($contractService);
                }
                $out = [
                    "status"    => 'success',
                    "message" => "input_success",
                ];
            } else {
                $out = [
                    "status"    => 'failed',
                    "message" => "input_failed",
                ];
            }
        }
        catch(\Exception $e)
        {
            //failed logic here
            DB::rollback();
            throw $e;
        }

        DB::commit();

        return response()->json($out);
    }

    public function detail(Request $request)
    {
        $contractId = $request->input("id");
        //var_dump($contractId);die();
        $query = Contract::where('id', $contractId)
                        ->with('segment:id,name')
                        ->with('user:id,name')
                        ->with('services')
                        ->with('services.service')
                        ->with('services.serviceType');

        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data,
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message"   => "Something Wrong",
                "data"      => []
            ];
        }

        return response()->json($out);
    }

    public function amandemen(Request $request)
    {
        $contractId = $request->input("contract_id");

        $contracNumber = $request->input("contract_number");
        $segment_id = $request->input("segment_id");
        $am_id = $request->input("am_id");
        $customerName = $request->input("customer_name");
        $npwp = $request->input("customer_npwp");
        $address = $request->input("customer_address");
        $pic1Name = $request->input("customer_pic1_name");
        $pic1Email = $request->input("customer_pic1_email");
        $pic1Position = $request->input("customer_pic1_position");
        $pic2Name = $request->input("customer_pic2_name");
        $pic2Email = $request->input("customer_pic2_email");
        $pic2Position = $request->input("customer_pic2_position");
        $duration = $request->input("duration");
        $startDate = $request->input("start_date");
        $endDate = $request->input("end_date");

        $services = $request->input("services");

        DB::beginTransaction();
        try{
            //var_dump($services);die();
            $customer = Customer::select("id")
                                ->where('name', $customerName)
                                ->where('npwp', $npwp)
                                ->first();

            if ($customer == null) {
                $dataCustomer = [
                    "name" => $customerName,
                    "npwp" => $npwp,
                    "address" => $address,
                    "pic_1_name" => $pic1Name,
                    "pic_1_email" => $pic1Email,
                    "pic_1_position" => $pic1Position,
                    "pic_2_name" => $pic2Name,
                    "pic_2_email" => $pic2Email,
                    "pic_2_position" => $pic2Position
                ];

                $customerId = Customer::create($dataCustomer)->id;
            }
            else {
                $customerId = $customer->id;
            }

            $dataContract = [
                "contract_number" => $contracNumber,
                "segment_id" => $segment_id,
                "user_id" => $am_id,
                "customer_id" => $customerId,
                "customer_name" => $customerName,
                "customer_npwp" => $npwp,
                "customer_address" => $address,
                "customer_pic1_name" => $pic1Name,
                "customer_pic1_email" => $pic1Email,
                "customer_pic1_position" => $pic1Position,
                "customer_pic2_name" => $pic2Name,
                "customer_pic2_email" => $pic2Email,
                "customer_pic2_position" => $pic2Position,
                "start_date" => $startDate,
                "end_date" => $endDate,
                "duration" => $duration
            ];

            $contract = Contract::find($contractId);
            $contract->services()->delete();

            if ($contract->update($dataContract)) {

                foreach($services as $value) {
                    $dataService = [
                        "sid" => $value['sid'],
                        "service_type_id" => $value['service_type_id'],
                        "service_id" => $value['service_id'],
                        "address" => array_key_exists('address', $value) ? $value['address'] : "",
                        "package" => $value['package'],
                        "bandwith_package" => $value['bandwith_package'],
                        "instalation_fee" => $value['instalation_fee'],
                        "monthly_fee" => $value['monthly_fee'],
                        "note" => $value['note'],
                    ];

                    $contractService = new ContractService($dataService);
                    $contract->services()->save($contractService);
                }
                $out = [
                    "status"    => 'success',
                    "message" => "update_success",
                ];
            } else {
                $out = [
                    "status"    => 'failed',
                    "message" => "update_failed",
                ];
            }
        }
        catch(\Exception $e)
        {
            //failed logic here
            DB::rollback();
            throw $e;
        }

        DB::commit();

        return response()->json($out);
    }

    public function recent()
    {
        $query = Contract::select('contract_number', 'activities', 'end_date')
                        ->latest("updated_at")
                        ->limit(5);

        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data,
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message"   => "Something Wrong",
                "data"      => []
            ];
        }

        return response()->json($out);
    }

    public function recentNew()
    {
        $query = Contract::latest("created_at")
                        ->limit(5);

        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data,
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message"   => "Something Wrong",
                "data"      => []
            ];
        }

        return response()->json($out);
    }

    public function reminder(Request $request)
    {
        $segmentId = $request->input('segment_id');
        $amId = $request->input('am_id');
        $keyword = $request->input('keyword');
        $token = $request->input('token');

        $user = User::where('token', $token)->first();
        $userId = $user->id;
        $segmentId = $user->segment_id;
        $level = $user->level;

        $query = Contract::with('segment:id,acronym')
                    ->with('user:id,name')
                    ->whereBetween('end_date', [Carbon::now()->toDateString(), Carbon::now()->subDays(-30)->toDateString()])
                    //->where('end_date', '>=', Carbon::now()->toDateTimeString())
                    //->where('end_date', '<=', Carbon::now()->subDays(-30)->toDateTimeString())
                    ->where(function($query) use ($keyword){
                      $query->where('contract_number', 'like', '%' . $keyword . '%');
                      $query->orwhereHas('services', function ($query) use($keyword){
                          return $query->where('sid', 'like', '%' . $keyword . '%');
                      });
                    })
                    ->where('is_do', '0')
                    ->orderBy('end_date');
        if ($level == 'ACCOUNT MANAGER') {
          $query = $query->where('user_id', $userId);
        }

        if ($level == 'MANAGER') {
          $query = $query->where('segment_id', $segmentId);
        }

        if ($amId != 0 || $amId != '') {
            $query = $query->where('user_id', $amId);
        }

        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data,
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message"   => "Something Wrong",
                "data"      => []
            ];
        }

        return response()->json($out);

    }

    public function do(Request $request)
    {
      $id = $request->input('id');

      $query = Contract::find($id);
      //var_dump($query);
      $query->end_date = date('Y-m-d');
      $query->is_do = true;

      if ($query->save()) {
          $out = [
              "status"    => 'success',
              "message" => 'success'
          ];
      }
      else {
          $out = [
              "status"    => 'failed',
              "message"   => "Something Wrong"
            ];
      }

      return response()->json($out);

    }

    public function service_do(Request $request)
    {
      $id = $request->input('id');

      $query = ContractService::find($id);
      //var_dump($query);
      //$query->end_date = date('Y-m-d');
      $query->is_do = true;

      if ($query->save()) {
        $out = [
            "status"    => 'success',
            "message" => 'success'
        ];

        $contract = Contract::find($query->contract_id);

        $allDo = false;

        foreach($contract->services as $key => $service) {
            if ($service->is_do) {
                $allDo = true;
            }
            else {
                $allDo = false;
            }
        }

        if ($allDo) {
            $query = Contract::find($query->contract_id);
            //var_dump($query);
            //$query->end_date = date('Y-m-d');
            $query->is_do = true;
            $query->save();
        }
      }
      else {
          $out = [
              "status"    => 'failed',
              "message"   => "Something Wrong"
            ];
      }

      return response()->json($out);

    }

    public function clearAmTestContract() {
      $user = User::where('email', 'am1test@example.com')->first();

      $contracts = Contract::where('user_id'. $user->id)->get();

      foreach ($contracts as $key => $contract) {
        $contract->services()->delete();
        $contract->destroy();
      }
    }
    //
}
