<?php

namespace App\Http\Controllers;

use App\Models\Segment;

class SegmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        $query = Segment::active();

        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message" => "Something Wrong",
                "data" => []
            ];
        }

        return response()->json($out);
    }

    //
}
