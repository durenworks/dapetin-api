<?php

namespace App\Http\Controllers;

use App\Models\Segment;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\Support\Facades\Response;

class DownloadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
      $file= public_path(). "/app-release.apk";
      $headers = [
              'Content-Type' => 'application/apk',
           ];
      $resp =  response()->download($file);
      
    $resp->headers->set('Expires', '0'); // Proxies.
    return $resp;
      //return Response::download($file);
      //$resp = response()->download("/app" . '/' . 'app-release.apk');
      //return response()->download(public_path('app-release.apk'));
      //return $resp;
      //return response()->file(storage_path('/app/' . 'app-release.apk'));
    }

    //
}
