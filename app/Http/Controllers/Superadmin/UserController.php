<?php

namespace App\Http\Controllers\Superadmin;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Models\Segment;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("superadmin");
    }

    public function index(Request $request)
    {
        $keyword = $request->input("keyword");
        $level = $request->input("level");
        $segment = $request->input("segment");

        $query = User::where('name', 'LIKE', '%' . $keyword . '%')
                        ->with('segment:id,acronym,name')
                        ->where('name', 'LIKE', '%' . $keyword . '%');
                        // ->orWhere('telegram_name', 'LIKE', '%' . $keyword . '%')
                        // ->orWhere('email', 'LIKE', '%' . $keyword . '%')
                        // ->orWhere('telegram_id', 'LIKE', '%' . $keyword . '%');
        if ($level != 'ALL') {
            $query = $query->where('level', '=', $level);
        }

        if ($segment != 'ALL') {
            $query = $query->where('segment', '=', $segment);
        }

        $query = $query->orderBy('is_active', 'desc');
        $query = $query->orderBy('name');

        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data,
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message"   => "Something Wrong",
                "data"      => []
            ];
        }

        return response()->json($out);
    }

    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'email' => 'required',
        //     'password' => 'required',
        //     'phone' => 'required',
        //     'nik' => 'required',
        //     'name' => 'required',
        //     'level' => 'required',
        //     'segment' => 'required',
        //     'telegram_name' => 'required',
        //     'telegram_id' => 'required',
        // ]);

        $email = $request->input("email");
        $password = $request->input("password");
        $nik = $request->input("nik");
        $name = $request->input("name");
        $phone = $request->input("phone");
        $level = $request->input("level");
        $segment_name = $request->input("segment");
        $telegram_name = $request->input("telegram_name");
        $telegram_id = $request->input("telegram_id");
        $witel = $request->input("witel");
        $hashPwd = Hash::make($password);

        $block = false;

        if (User::where('email', '=', $email)->exists()) {
            $block = true;

            $out = [
                "status"    => 'failed',
                "message" => "Email Already Registered",
            ];
         }

         if (!$block && (User::where('nik', '=', $nik)->exists())) {
            $block = true;

            $out = [
                "status"    => 'failed',
                "message" => "Nik Already Registered",
            ];
         }

         if (!$block && (User::where('phone', '=', $phone)->exists())) {
            $block = true;

            $out = [
                "status"    => 'failed',
                "message" => "Phone Already Registered",
            ];
         }

         $segmentId = null;
         if ($level != "SUPERADMIN") {
           if (!$block && (User::where('telegram_id', '=', $telegram_id)->exists())) {
              $block = true;

              $out = [
                  "status"    => 'failed',
                  "message" => "Telegram ID Already Registered",
              ];
           }

           $segment = Segment::where('name', $segment_name)
                           ->first();
           $segmentId = $segment->id;
         }


        $data = [
            "email" => $email,
            "password" => $hashPwd,
            'nik' => $nik,
            'name' => $name,
            'phone' => $phone,
            'level' => $level,
            'segment_id' => $segmentId,
            'telegram_name' => $telegram_name,
            'telegram_id' => $telegram_id,
            'witel' => $witel
        ];

//var_dump($data);die();
        if (!$block) {
            if (User::create($data)) {
                $out = [
                    "status"    => 'success',
                    "message" => "input_success",
                ];
            } else {
                $out = [
                    "status"    => 'failed',
                    "message" => "input_failed",
                ];
            }
        }

        return response()->json($out);
    }

    public function detail(Request $request)
    {
        $id = $request->input("id");

        $query = User::where('id', $id)
                        ->with("segment:id,name");
        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data,
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message" => "Something Wrong",
                "data" => []
            ];
        }

        return response()->json($out);
    }

    public function update(Request $request)
    {
        $id = $request->input("id");
        $email = $request->input("email");
        $password = $request->input("password");
        $nik = $request->input("nik");
        $name = $request->input("name");
        $phone = $request->input("phone");
        $level = $request->input("level");
        $witel = $request->input("witel");
        $segment_name = $request->input("segment");
        $telegram_name = $request->input("telegram_name");
        $telegram_id = $request->input("telegram_id");

        $hashPwd = '';
        if ($password != '' || $password != null) {
          $hashPwd = Hash::make($password);
        }

        //var_dump($segment->id);

        $block = false;

        if ($id == '') {
            $block = true;

            $status = 'failed';
            $message = 'ID Is Empty';
        }

        if (!$block && (User::where('email', '=', $email)
                ->where('id', '<>', $id)
                ->exists())) {
            $block = true;

            $status = 'failed';
            $message = 'Email Already Registered';
         }

         if (!$block && (User::where('nik', '=', $nik)
                ->where('id', '<>', $id)
                ->exists())) {
            $block = true;

            $block = true;

            $status = 'failed';
            $message = 'NIK Already Registered';
         }

         if (!$block && (User::where('phone', '=', $phone)
                    ->where('id', '<>', $id)
                    ->exists())) {
            $block = true;

            $status = 'failed';
            $message = 'Phone Already Registere';
         }

         $segmentId = null;
         if ($level != "SUPERADMIN") {
           if (!$block && (User::where('telegram_id', '=', $telegram_id)
                  ->where('id', '<>', $id)
                  ->exists())) {
              $block = true;

              $status = 'failed';
              $message = 'Telegram ID Already Registered';
           }

           $segment = Segment::where('name', $segment_name)
                           ->first();
           $segmentId = $segment->id;
         }


        if ($hashPwd == '') {
          $data = [
              "email" => $email,
              'nik' => $nik,
              'name' => $name,
              'phone' => $phone,
              'level' => $level,
              'segment_id' => $segmentId,
              'telegram_name' => $telegram_name,
              'telegram_id' => $telegram_id,
              'witel' => $witel
          ];
        }
        else {
          $data = [
              "email" => $email,
              "password" => $hashPwd,
              'nik' => $nik,
              'name' => $name,
              'phone' => $phone,
              'level' => $level,
              'segment_id' => $segmentId,
              'telegram_name' => $telegram_name,
              'telegram_id' => $telegram_id,
              'witel' => $witel
          ];
        }


        if (!$block) {
            if (User::where('id', $id)
                        ->update($data)) {
                $status = 'success';
                $message = 'Update Success';

            } else {
                $status = 'failed';
                $message = 'Something Wrong';
            }
        }

        $out = [
            "status"    => $status,
            "message" => $message,
        ];

        return response()->json($out);
    }

    public function delete(Request $request)
    {
        $id = $request->input("id");


        $block = false;
        if ($id == '') {
            $block = true;

            $status = 'failed';
            $message = 'ID Is Empty';
        }

        $data = [
            "is_active" => false
        ];

        if (!$block) {
            if (Contract::where('user_id', $id)->exists() || ($id == 3)) {
                if (User::where('id', $id)
                            ->update($data)) {
                    $status = 'success';
                    $message = 'Delete Success';

                } else {
                    $status = 'failed';
                    $message = 'Something Wrong';
                }
            }
            else {
                if (User::destroy($id)) {
                    $status = 'success';
                    $message = 'Delete Success';

                } else {
                    $status = 'failed';
                    $message = 'Something Wrong';
                }
            }
        }

        $out = [
            "status"    => $status,
            "message" => $message,
        ];

        return response()->json($out);
    }

    public function restore(Request $request)
    {
        $id = $request->input("id");


        $block = false;
        if ($id == '') {
            $block = true;

            $status = 'failed';
            $message = 'ID Is Empty';
        }

        $data = [
            "is_active" => true
        ];

        if (!$block) {
            if (User::where('id', $id)
                        ->update($data)) {
                $status = 'success';
                $message = 'Restore Success';

            } else {
                $status = 'failed';
                $message = 'Something Wrong';
            }
        }

        $out = [
            "status"    => $status,
            "message" => $message,
        ];

        return response()->json($out);
    }

    public function clearUser() {
        $users = user::where('is_active', 0)->get();

        foreach($users as $user) {
            if (!Contract::where('user_id', $user->id)->exists()) {
                User::destroy($user->id);
            }
        }

        $user = User::where('email', 'am1test@example.com')->first();

        $contracts = Contract::where('user_id', $user->id)->get();

        foreach ($contracts as $key => $contract) {
          $contract->services()->delete();
          $contract->delete();
        }

        $user = User::where('email', 'am2test@example.com')->first();

        $contracts = Contract::where('user_id', $user->id)->get();

        foreach ($contracts as $key => $contract) {
          $contract->services()->delete();
          $contract->delete();
        }

        $contracts = Contract::where('contract_number', '<>', '')->get();

        foreach ($contracts as $key => $contract) {
          // code...
          $contracts2 = Contract::where('contract_number', '=', $contract->contract_number)
                                  ->where('id', '<>', $contract->id)
                                  ->get();
          foreach ($contracts2 as $key => $contract2) {
            // code...
            $contract->services()->delete();
            $contract->delete();
          }
        }
    }

    public function restoreUser() {
        $users = user::where('is_active', 0)->get();

        foreach($users as $user) {
            $user->is_active = 1;
            $user->save();
        }
    }

}
