<?php

namespace App\Http\Controllers\Superadmin;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Service;

class ServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("superadmin");
    }

    public function index(Request $request)
    {
        $keyword = $request->input("keyword");

        $query = Service::where(function($query) use ($keyword){
                            $query->where('type', 'LIKE', '%' . $keyword . '%');
                            $query->where('name', 'LIKE', '%' . $keyword . '%');
                            $query->where('package', 'LIKE', '%' . $keyword . '%');
                            $query->where('partner', 'LIKE', '%' . $keyword . '%');
                        })
                        ->active();

        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data,
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message" => "Something Wrong",
                "data" => [],
            ];
        }

        return response()->json($out);
    }

    public function store(Request $request)
    {
        $type = $request->input("type");
        $name = $request->input("name");
        $package = $request->input("package");
        $partner = $request->input("partner");

        $block = false;

        if ($type == '') {
            $block = true;

            $out = [
                "status"    => 'failed',
                "message" => "Type Is Empty",
            ];
         }

         if ($name == '') {
            $block = true;

            $out = [
                "status"    => 'failed',
                "message" => "Name Is Empty",
            ];
         }

         if ($package == '') {
            $block = true;

            $out = [
                "status"    => 'failed',
                "message" => "Package Is Empty",
            ];
         }

         if ($partner == '') {
            $block = true;

            $out = [
                "status"    => 'failed',
                "message" => "Partner Is Empty",
            ];
         }

        if (!$block) {
            $this->validate($request, [
                'type' => 'required',
                'name' => 'required',
                'package' => 'required',
                'partner' => 'required'
            ]);

            $data = [
                "type" => $type,
                "name" => $name,
                'package' => $package,
                'partner' => $partner
            ];

            if (Service::create($data)) {
                $out = [
                    "status"    => 'success',
                    "message" => "input_success",
                ];
            } else {
                $out = [
                    "status"    => 'failed',
                    "message" => "Something Worong",
                ];
            }
        }

        return response()->json($out);
    }

    public function detail(Request $request)
    {
        $id = $request->input("id");

        if ($data = Service::find($id)) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message" => "Something Wrong",
                "data" => []
            ];
        }

        return response()->json($out);
    }

    public function update(Request $request
    )
    {
        $id = $request->input("id");
        $type = $request->input("type");
        $name = $request->input("name");
        $package = $request->input("package");
        $partner = $request->input("partner");

        $block = false;

        if ($id == '') {
            $block = true;

            $status = 'failed';
            $message = 'ID Is Empty';
        }

        if ($type == '') {
            $block = true;

            $status = 'failed';
            $message = "Type Is Empty";
         }

         if ($name == '') {
            $block = true;

            $status = 'failed';
            $message = "Name Is Empty";
         }

         if ($package == '') {
            $block = true;

            $status = 'failed';
            $message = "Package Is Empty";
         }

         if ($partner == '') {
            $block = true;

            $status = 'failed';
            $message = "Partner Is Empty";
         }

        if (!$block) {
            $this->validate($request, [
                'type' => 'required',
                'name' => 'required',
                'package' => 'required',
                'partner' => 'required'
            ]);

            $data = [
                "type" => $type,
                "name" => $name,
                'package' => $package,
                'partner' => $partner
            ];

            if (Service::where('id', $id)
                        ->update($data)) {
                $status = 'success';
                $message = 'Update Success';

            } else {
                $status = 'failed';
                $message = 'Something Wrong';
            }
        }

        $out = [
            "status"    => $status,
            "message" => $message,
        ];

        return response()->json($out);
    }

    public function delete(Request $request)
    {
        $id = $request->input("id");

        $block = false;
        if ($id == '') {
            $block = true;

            $status = 'failed';
            $message = 'ID Is Empty';
        }

        $data = [
            "is_active" => false
        ];

        if (!$block) {
            if (Service::where('id', $id)
                        ->update($data)) {
                $status = 'success';
                $message = 'Delete Success';

            } else {
                $status = 'failed';
                $message = 'Something Wrong';
            }
        }

        $out = [
            "status"    => $status,
            "message" => $message,
        ];

        return response()->json($out);
    }

}
