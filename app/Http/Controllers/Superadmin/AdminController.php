<?php

namespace App\Http\Controllers\Superadmin;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("superadmin");
    }

    public function index(Request $request)
    {
        $keyword = $request->input("keyword");

        $query = User::where('level', '=', 'SUPERADMIN')
                        ->where(function($query) use ($keyword){
                            $query->where('name', 'LIKE', '%' . $keyword . '%');
                            $query->where('email', 'LIKE', '%' . $keyword . '%');
                        })
                        ->active();

        if ($data = $query->get()) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message" => "Something Wrong",
                "data" => []
            ];
        }

        return response()->json($out);
    }

    public function store(Request $request)
    {
        $email = $request->input("email");
        $password = $request->input("password");
        $nik = $request->input("nik");
        $name = $request->input("name");
        $level = 'SUPERADMIN';
        $phone = $request->input("phone");
        $telegram_name = $request->input("telegram_name");
        $telegram_id = $request->input("telegram_id");
        $hashPwd = Hash::make($password);

        $block = false;

        if (User::where('email', '=', $email)->exists()) {
            $block = true;

            $status   = 'failed';
            $message = "Email Already Registered";
         }

         if (!$block && (User::where('phone', '=', $phone)->exists())) {
            $block = true;

            $status   = 'failed';
            $message = "Phone Already Registered";
         }

         if (!$block && (User::where('nik', '=', $nik)->exists())) {
            $block = true;

            $status   = 'failed';
            $message = "NIK Already Registered";
         }

         /*if (!$block && (User::where('telegram_name', '=', $telegram_name)->exists())) {
            $block = true;

            $tatus   = 'failed';
            $message = "Telegram Name Already Registered";
         }

         if (!$block && (User::where('telegram_id', '=', $telegram_id)->exists())) {
            $block = true;

            $tatus   = 'failed';
            $message = "Telegram ID Already Registered";
         }*/

        $data = [
            "email" => $email,
            "password" => $hashPwd,
            'nik' => $nik,
            'name' => $name,
            'phone' => $phone,
            'level' => $level,
            //'telegram_name' => $telegram_name,
            //'telegram_id' => $telegram_id,
        ];

        if (!$block) {
          //var_dump('im in');
            if (User::create($data)) {
              // /var_dump('im inside');
                $status     = 'success';
                $message    = 'input_success';
            } else {
                $status    = 'failed';
                $message = 'input_failed';
            }
        }

        $out = [
            "status"    => $status,
            "message" => $message,
        ];

        return response()->json($out);
    }

    public function detail(Request $request)
    {
        $id = $request->input("id");

        if ($data = User::find($id)) {
            $out = [
                "status"    => 'success',
                "message" => 'success',
                "data" => $data,
            ];
        }
        else {
            $out = [
                "status"    => 'failed',
                "message" => "Something Wrong",
                "data" => [],
            ];
        }

        return response()->json($out);
    }

    public function update(Request $request
    )
    {
        $id = $request->input("id");
        $email = $request->input("email");
        $password = $request->input("password");
        $nik = $request->input("nik");
        $name = $request->input("name");
        $phone = $request->input("phone");
        $telegram_name = $request->input("telegram_name");
        $telegram_id = $request->input("telegram_id");

        $hashPwd = '';
        if ($password <> '') {
            $hashPwd = Hash::make($password);
        }

        $block = false;

        if ($id == '') {
            $block = true;

            $status = 'failed';
            $message = 'ID Is Empty';
        }

        if (!$block && (User::where('email', '=', $email)
                ->where('id', '<>', $id)
                ->exists())) {
            $block = true;

            $status = 'failed';
            $message = 'Email Already Registered';
        }

        if (!$block && (User::where('phone', '=', $phone)
                    ->where('id', '<>', $id)
                    ->exists())) {
            $block = true;

            $status = 'failed';
            $message = 'Phone Already Registered';
        }

        if (!$block && (User::where('nik', '=', $nik)
                    ->where('id', '<>', $id)
                    ->exists())) {
            $block = true;

            $status = 'failed';
            $message = 'NIK Already Registered';
        }

        /*if (!$block && (User::where('telegram_name', '=', $telegram_name)
                    ->where('id', '<>', $id)
                    ->exists())) {
            $block = true;

            $status = 'failed';
            $message = 'Telegram Name Already Registered';
        }

        if (!$block && (User::where('telegram_id', '=', $telegram_id)
                    ->where('id', '<>', $id)
                    ->exists())) {
            $block = true;

            $status = 'failed';
            $message = 'Telegram ID Already Registered';
        }*/
        $data = [
            "email" => $email,
            "password" => $hashPwd,
            'nik' => $nik,
            'name' => $name,
            'phone' => $phone,
            //'telegram_name' => $telegram_name,
            //'telegram_id' => $telegram_id,
        ];

        if ($hashPwd == '') {
            unset($data['password']);
        }

        if (!$block) {
            if (User::where('id', $id)
                        ->update($data)) {
                $status = 'success';
                $message = 'Update Success';

            } else {
                $status = 'failed';
                $message = 'Something Wrong';
            }
        }

        $out = [
            "status"    => $status,
            "message" => $message,
        ];

        return response()->json($out);
    }

    public function delete(Request $request)
    {
        $id = $request->input("id");

        $block = false;
        if ($id == '') {
            $block = true;

            $status = 'failed';
            $message = 'ID Is Empty';
        }

        $data = [
            "is_active" => false
        ];

        if (!$block) {
            if (User::where('id', $id)
                        ->update($data)) {
                $status = 'success';
                $message = 'Delete Success';

            } else {
                $status = 'failed';
                $message = 'Something Wrong';
            }
        }

        $out = [
            "status"    => $status,
            "message" => $message,
        ];

        return response()->json($out);
    }

}
