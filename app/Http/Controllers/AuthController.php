<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class AuthController extends Controller
{

    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:users|max:255',
            'password' => 'required|min:6'
        ]);

        $email = $request->input("email");
        $password = $request->input("password");

        $hashPwd = Hash::make($password);

        $data = [
            "email" => $email,
            "password" => $hashPwd
        ];



        if (User::create($data)) {
            $out = [
                "message" => "register_success",
                "code"    => 201,
            ];
        } else {
            $out = [
                "message" => "vailed_regiser",
                "code"   => 404,
            ];
        }

        return response()->json($out, $out['code']);
    }

    public function login(Request $request)
    {

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:6'
        ]);

        $email = $request->input("email");
        $password = $request->input("password");

        $user = User::where("email", $email)->first();

        if (!$user) {
            $out = [
                "status" => "failed",
                "message" => "Ivalid Username or Password",
                "data"  => [
                    "token" => null,
                ]
            ];
            return response()->json($out);
        }

        if (Hash::check($password, $user->password)) {
            if ($user->token == "") {
              $newtoken  = $this->generateRandomString();

              $user->update([
                  'token' => $newtoken
              ]);
            }
            else {
              $newtoken = $user->token;
            }

            $out = [
                "status" => "success",
                "message" => "Login Success",
                "data"  => [
                    "token" => $newtoken,
                    "level" => $user->level
                ]
            ];
        } else {
            $out = [
                "status" => "success",
                "message" => "Something Wrong",
                "data"  => [
                    "token" => null,
                ]
            ];
        }

        return response()->json($out);
    }

    function generateRandomString($length = 80)
    {
        $char = '012345678dssd9abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $char_length = strlen($char);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $char[rand(0, $char_length - 1)];
        }
        return $str;
    }

    function get_profile(Request $request)
    {
        $token = $request->input("token");
        $user = User::where("token", $token)->first();

        if (!$user) {
            $out = [
                "status" => "failed",
                "message" => "Ivalid Token",
                "data"  => [
                    "token" => null,
                ]
            ];
        }
        else {
            $out = [
                "status" => "success",
                "message" => "sucess",
                "data"  => $user
            ];
        }

        return response()->json($out);
    }
}
