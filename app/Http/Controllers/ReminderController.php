<?php

namespace App\Http\Controllers;

ini_set('memory_limit', '512M');

use Illuminate\Http\Request;
use App\Models\Contract;
use App\Http\Controllers\Controller;
use App\Models\ContractService;
use App\Models\Customer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Support\Facades\Mail;

class ReminderController extends Controller
{
    public function index($emailOn, $telegramOn)
    {
      var_dump($telegramOn);

      if ($emailOn == 1) {
        echo "Email ON\n";
      }
      else {
        echo "Email OFF\n";
      }

      if ($telegramOn == 1) {
        echo "Telegram ON\n";
      }
      else {
        echo "Telegram OFF\n\n";
      }

      $query = Contract::with('segment:id,acronym')
                    ->with('user:id,name,telegram_id,email')
                    ->with('segment:id,name,acronym')
                    ->with('services')
                    ->where('end_date', '<=', Carbon::now()->subDays(-30)->toDateTimeString())
                    ->where('end_date', '>=', Carbon::now()->toDateTimeString())
                    ->where('is_do', '0');

        $contracts = $query->get();

        foreach ($contracts as $key => $contract) {
          $telegramId = $contract->user->telegram_id;
          $email = $contract->user->email;
          $name = $contract->user->name;
          $contractNumber = $contract->contract_number;
          $segment = $contract->segment->name;
          $custEmail1 = $contract->customer_pic1_email;
          $custEmail2 = $contract->customer_pic2_email;
          $customerName = $contract->customer_name;
          $customerAddress = $contract->customer_address;
          $customerNPWP = $contract->customer_npwp;
          $customerPIC1Name = $contract->customer_pic1_name;
          $customerPIC1Position = $contract->customer_pic1_position;
          $endDate = $contract->end_date_indo;
          $expired = $contract->end_date;
          $now = Carbon::now();
          $difference = $expired->diff($now)->days;

          //var_dump($contract->services);
          $message =
             "🔴 REMINDER KONTRAK 🔴
              Selamat pagi rekan AM ers yang keren, jangan lupa yaa untuk perpanjangan kontrak pelanggan ini..

              Nomor Kontrak : " . $contractNumber . "
              Segmen : " . $segment . "
              Nama Pelanggan : " . $customerName . "
              Alamat Pelanggan : " . $customerAddress . "
              NPWP Pelanggan : " . $customerNPWP . "
              Nama TTD Kontrak :  " . $customerPIC1Name . "
              Jabatan TTD Kontrak :  " . $customerPIC1Position . "
              Tanggal Habis Masa Kontrak :  " . $endDate . " \n\n";

              foreach ($contract->services as $key => $service) {
                $serviceName = $service->service->name;
                $bandwith = $service->bandwith_package;
                $sid = $service->sid;

                $serviceString = $serviceName . " | " . $bandwith . " | " . $sid ."\n";

                $message = $message . $serviceString;
              }


            $message = $message .   "\nYuk di siapakan perpanjangan kontraknya, karena masa kontrak sisa " . $difference . " HARI LAGI !!!!!";

            var_dump("nomor kontrak = " . $contractNumber);
            var_dump("telegram id = " . $telegramId);
            var_dump("email = " . $email);
            var_dump("email Customer = " . $custEmail1);
            echo "++++++++++++++++++++++++++++++++++++++++++++++++++\n";
            echo "=========================================+++++++++\n";
            echo "++++++++++++++++++++++++++++++++++++++++++++++++++\n";
            if ($telegramOn == 1) {
              try {
                Telegram::sendMessage([
                    'chat_id' => '1117901595',
                    'text' => $message
                ]);
              }
              catch(\Exception $e){
              }

              try {
                Telegram::sendMessage([
                    'chat_id' => $telegramId,
                    'text' => $message
                ]);
              }
              catch(\Exception $e){
              }

            }

            if ($emailOn == 1) {
              if ($email <> 'am1test@example.com' || $email <> 'am2test@example.com') {
                try {
                  Mail::send('email.reminder', ['data' => $contract], function ($message) use ($email, $name) {
                      $message->to($email, $name)->subject("Contract Reminder");
                  });
                }
                catch(\Exception $e){
                }

                if ($custEmail1 != '') {
                  try {
                    Mail::send('email.reminder_customer', ['data' => $contract], function ($message) use ($custEmail1, $name) {
                        $message->to($custEmail1, $name)->subject("Contract Reminder");
                    });
                  }
                  catch(\Exception $e){
                  }
                }
              }
            }
        }
    }

    public function email()
    {
        $data = [];
        Mail::send('email.reminder_test', ['data' => null], function ($message) {
            $message->to('dapetin.id20@gmail.com', 'dadittya')->subject("Welcome!");
        });
    }

    //
}
