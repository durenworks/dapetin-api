<?php

/**
* Location: /app/Http/Middleware
*/
namespace App\Http\Middleware;

use Closure;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
         //header("Access-Control-Allow-Origin: *");
         //ALLOW OPTIONS METHOD
         $headers = [
             'Access-Control-Allow-Methods' => 'GET, POST, PATCH, DELETE, PUT, OPTIONS',
             'Access-Control-Allow-Headers' => 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
         ];
         if ($request->getMethod() == "OPTIONS"){
             //The client-side application can set only headers allowed in Access-Control-Allow-Headers
             return response()->json('OK',200,$headers);
         }
         $response = $next($request);
         foreach ($headers as $key => $value) {
             $response->header($key, $value);
         }
         return $response;

     }
}
