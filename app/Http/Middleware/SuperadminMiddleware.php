<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class SuperadminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*if ($request->input('token')) {
            $check =  User::where('token', $request->input('token'))
                            ->where('level', 'SUPERADMIN')
                            ->first();

            if (!$check) {
                $out = [
                    "status"    => 'failed',
                    "message" => "Invalid Token",
                ];
                return response($out);
            } else {
                return $next($request);
            }
        } else {
            $out = [
                "status"    => 'failed',
                "message" => "Invalid Token",
            ];
            return response($out, 401);
        }*/
        return $next($request);
    }
}
