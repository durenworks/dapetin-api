<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    protected $fillable = [
        'name'
    ];

    protected $hidden = [
        'is_active',
        'updated_at',
        'created_at'
    ];

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    public function services() {
      return $this->hasMany('App\Models\Service');
    }
}
