<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $fillable = [
        'contract_number',
        'segment_id',
        'user_id',
        'customer_id',
        'customer_npwp',
        'customer_name',
        'customer_address',
        'customer_pic1_name',
        'customer_pic1_position',
        'customer_pic1_email',
        'customer_pic2_name',
        'customer_pic2_position',
        'customer_pic2_email',
        'customer_pic2_phone',
        'customer_pic1_phone',
        'start_date',
        'end_date',
        'duration',
        'activities',
    ];

    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    protected $dates = [
        'start_date',
        'end_date'
    ];

    protected $appends = ['end_date_indo', 'remaining', 'remaining_days', 'service_list'];

    public function services()
    {
        return $this->hasMany('App\Models\ContractService');
    }

    public function segment()
    {
        return $this->belongsTo('App\Models\Segment');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function getEndDateIndoAttribute()
    {
        return $this->end_date->format('j M Y');
        //return 'foo';
    }

    public function getRemainingAttribute()
    {
        $expired = $this->end_date;
        $now = Carbon::now();
        $difference = $expired->diff($now)->days + 1;

          $text = "> 1 Tahun";

          if ($difference < 1) {
            $text = "Do";
          }
          if ($difference == 1) {
              $text = "< 1 Hari";
          }
          else if ($difference <= 3) {
              $text = "< 3 Hari";
          }
          else if ($difference <= 7) {
              $text = "< 1 Minggu";
          }
          else if ($difference <= 21) {
              $text = "< 3 Minggu";
          }
          else if ($difference <= 30) {
              $text = "< 1 Bulan";
          }
          else if ($difference <= 60) {
              $text = "1 - 2 Bulan";
          }
          else if ($difference <= 90) {
              $text = "2 - 3 Bulan";
          }
          else if ($difference <= 120) {
              $text = "3 - 4 Bulan";
          }
          else if ($difference <= 150) {
              $text = "4 - 5 Bulan";
          }
          else if ($difference <= 180) {
              $text = "5 - 6 Bulan";
          }
          else if ($difference <= 356) {
              $text = "6 - 12 Bulan";
          }

        return "{$text}";
    }

    public function getRemainingDaysAttribute()
    {
        $expired = $this->end_date;
        $now = Carbon::now();
        $difference = $expired->diff($now)->days + 1;
        if ($expired < $now) {
            $text = "0 hari";
        }
        else {
            $text = $difference . " hari";
        }

        return "{$text}";
    }

    public function getServiceListAttribute()
    {
      $strService = "";
      foreach ($this->services()->get() as $key => $service) {
        $strService = $strService . $service->sid . "/n/n";
      }
      if (substr($strService, -2) == "/n") {
        $strService = substr($strService, 0, -4);
      }

      $strService = str_replace("/n/n", "\r\n", $strService);
      return $strService;
    }
}
