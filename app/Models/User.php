<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $fillable = [
        'nik',
        'email',
        'password',
        'name',
        'phone',
        'level',
        'segment_id',
        'telegram_name',
        'telegram_id',
        'witel',
        'last_active',
        'is_active',
        'token'
    ];

    protected $hidden = [
        'password'
    ];

    protected $dates = [
        'last_active'
    ];

    public function segment()
    {
        return $this->belongsTo('App\Models\Segment');
    }

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

}
