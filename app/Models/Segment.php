<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Segment extends Model
{
    protected $fillable = [
        'name',
        'acronym'
    ];

    protected $hidden = [
        'is_active',
        'updated_at',
        'created_at'
    ];

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
}
