<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'service_type_id',
        'name'
    ];

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    protected $hidden = [
        'is_active',
        'updated_at',
        'created_at'
    ];

    public function ServiceType()
    {
        return $this->belongsTo('App\Models\ServiceType');
    }
}
