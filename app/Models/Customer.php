<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'name',
        'npwp',
        'address',
        'pic_1_name',
        'pic_1_position',
        'pic_1_email',
        'pic_1_phone',
        'pic_2_name',
        'pic_2_position',
        'pic_2_phone'

    ];

    protected $hidden = [
        'is_active',
        'updated_at',
        'created_at'
    ];

    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    public function contracts()
    {
        return $this->hasMany('App\Models\Contract');
    }
}
