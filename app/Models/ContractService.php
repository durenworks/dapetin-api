<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContractService extends Model
{
    protected $fillable = [
        'sid',
        'contract_id',
        'service_type_id',
        'service_id',
        'package',
        'bandwith_package',
        'instalation_fee',
        'monthly_fee',
        'note',
        'address'
    ];

    protected $hidden = [
        'updated_at',
        'created_at'
    ];

    public function contract()
    {
        return $this->belongsTo('App\Models\Contract');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function serviceType()
    {
        return $this->belongsTo('App\Models\ServiceType');
    }
}
